#include <iostream>
using namespace std;

int main() {
    int a, b, sum;
    cout << "Podaj pierwsza liczbe: " << endl;
    cin >> a;
    cout << "Podaj druga liczbe: " << endl;
    cin >> b;
    sum = a + b;
    cout << "Pierwsza liczba to: " << a << endl;
    cout << "Druga liczba to: " << b << endl;
    cout << "Suma tych liczb to: " << sum << endl;
    return 0;
}